﻿Import-Module mti.adsi

$printers = get-content -raw \\marine-travelift.com\NETLOGON\Scripts\Logon_MapPrinters.json | ConvertFrom-Json

If($(Test-ADSIComputerIsGroupMember -_GroupName "COMPUTER-NoPrinterScript") -eq $false){
    foreach($printer in $printers.printers){
        If(Test-ADSIUserIsGroupMember -_GroupName $printer.Name){
            IF($null -eq $(get-printer -Name "\\SVRPRT01\$($printer.path)" -ErrorAction SilentlyContinue)){
                Add-Printer -ConnectionName "\\SVRPRT01\$($printer.path)"
                Write-Host "Adding $($printer.path)..."
            }
        }

        If($(Test-ADSIUserIsGroupMember -_GroupName $printer.Name) -eq $false){
            IF($null -ne $(get-printer -Name "\\SVRPRT01\$($printer.path)" -ErrorAction SilentlyContinue)){
                Remove-Printer -Name "\\SVRPRT01\$($printer.path)"
                Write-Host "Removing $($printer.path)..."
            }
        }
    }
}